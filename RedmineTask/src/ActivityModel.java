
public class ActivityModel {
	
	private int actID;
	private String actName;
	private String actTimeIn;
	private String actTimeOut;
	private String actCategory;
	
	
	public int getActID() {
		return actID;
	}
	public void setActID(int actID) {
		this.actID = actID;
	}
	public String getActName() {
		return actName;
	}
	public void setActName(String actName) {
		this.actName = actName;
	}
	public String getActTimeIn() {
		return actTimeIn;
	}
	public void setActTimeIn(String actTime) {
		this.actTimeIn = actTime;
	}
	
	public String getActTimeOut() {
		return actTimeOut;
	}
	public void setActTimeOut(String actTimeOut) {
		this.actTimeOut = actTimeOut;
	}
	
	public String getActCategory() {
		return actCategory;
	}
	public void setActCategory(String actCategory) {
		this.actCategory = actCategory;
	}
	
}
