import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.border.Border;


public class lolProject {

	private JFrame frame;
	private JButton btnLogin;
	private JTextArea txtInput;

	public static void main(String[] args) {
		lolProject window = new lolProject();
		window.frame.setVisible(true);
	}

	public lolProject() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		btnLogin = new JButton();
		txtInput = new JTextArea();
		
		btnLogin.setText("LOL");
		btnLogin.setSize(new Dimension(10, 10));
		
		txtInput.setSize(new Dimension(100, 20));
		frame.setLayout(null);
		
		Dimension btnDim = btnLogin.getPreferredSize();
		Dimension txtDim = txtInput.getPreferredSize();
		
		Insets insets = frame.getInsets();
		btnLogin.setBounds(insets.left + 10, insets.top + 10, btnDim.width, btnDim.height);
		
		frame.add(txtInput);
		//frame.add(btnLogin);
		frame.setSize(200, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
