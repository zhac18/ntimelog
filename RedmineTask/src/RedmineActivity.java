import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.StringTokenizer;

public class RedmineActivity {

	private static Scanner write = new Scanner(System.in);
	private static UserInput user;
	private static String userName = "";
	private final static String INVALID_USER_INPUT = "Invalid User Input!";
	private final static String WELCOME_NOTE = "Welcome,";
	private final static String HELP_NOTE = "Kindly type [help] for the following commands.\n";

	private final static int INDEX_NAME = 0, INDEX_CATEGORY = 1,
			INDEX_TIMEIN = 2, INDEX_TIMEOUT = 3;

	private final static String CMD_HELP = "help";
	private final static String CMD_ACT_ADD = "activity -add";
	private final static String CMD_EXIT = "exit";


	public static void start() {
		GenerateSampleOutput();
		initComponent();
	}

	private static void initComponent() {

		String mainInput = "";

		while (Initialization()) {
			System.out.println(userName);
		}

		ExecuteShowAllActivity();

		while (!mainInput.equals(CMD_EXIT)) {
			System.out.print(userName);
			mainInput = write.nextLine();
			String modifiedInput = TrimSpaces(mainInput).toLowerCase();
			String validatedInput = validateInput(modifiedInput);

			switch (validatedInput) {

			case CMD_HELP:
				PrintCommands();
				break;
			case CMD_ACT_ADD:
				ExecuteAddActivity(modifiedInput);
				break;
			case CMD_EXIT:
				System.out.println("Thanks for using the application :)");
				break;
			default:
				PrintInvalidCommand();
			}

		}

	}

	// <Command Execution>

	private static void ExecuteShowAllActivity() {
		System.out.println();
		for (ActivityModel model : ActivityList.getActivityList()) {
			System.out.printf("%s - %s - %s - %s \n", model.getActName(),
					model.getActCategory(), model.getActTimeIn(),
					model.getActTimeOut());
		}
		System.out.println();
		System.out.printf("Total results: %d \n", ActivityList
				.getActivityList().size());
		System.out.println();
	}

	private static void ExecuteAddActivity(String Command) {
		ArrayList<String> catcher = new ArrayList<String>();
		String nameSeparator = Command.replace(CMD_ACT_ADD, "").trim();
		StringTokenizer token = new StringTokenizer(nameSeparator, ",");
		String actName, actCat, actTimeIn, actTimeOut;

		while (token.hasMoreTokens()) {
			catcher.add(token.nextToken());
		}

		if (catcher.size() == 4) {

			actName = catcher.get(INDEX_NAME);
			actCat = catcher.get(INDEX_CATEGORY);
			actTimeIn = catcher.get(INDEX_TIMEIN);
			actTimeOut = catcher.get(INDEX_TIMEOUT);

			ActivityList.InsertEntryInActivity(actName, actCat, actTimeIn,actTimeOut);
			PrintSuccessfulEntry(actName, actCat, actTimeIn, actTimeOut);
			ExecuteShowAllActivity();
		} else {
			System.out.println("Invalid input!");
		}

	}

	// </Command Execution>

	// <Command Validators>

	private static String validateInput(String Input) {
		String value = "";
		if (Input.indexOf(CMD_HELP) > -1) {
			value = CMD_HELP;
		} else if (Input.indexOf(CMD_EXIT) > -1) {
			value = CMD_EXIT;
		} else if (Input.indexOf(CMD_ACT_ADD) > -1) {
			value = CMD_ACT_ADD;
		}

		return value;
	}

	private static boolean Initialization() {
		boolean verification = true;
		String userInputName = "";

		System.out.print("Input Username: ");
		userInputName = write.nextLine();
		user = new UserInput(userInputName);

		if (user.validate()) {
			userName = user.displayHeader();
			System.out.printf("%s user %s!\n", WELCOME_NOTE, userInputName);
			System.out.print(HELP_NOTE);
			verification = false;
		} else {
			System.out.print(INVALID_USER_INPUT);
		}

		return verification;
	}

	private static String TrimSpaces(String Input) {
		String trimInput = Input.trim();
		while (trimInput.indexOf("  ") > -1) {
			trimInput = trimInput.replace("  ", " ");
		}
		return trimInput;
	}

	// <//Command Validators>

	// <Printing methods>

	private static void PrintInvalidCommand() {
		System.out.print("Invalid Command!\n");
	}

	private static void PrintCommands() {
		System.out.println();
		System.out
				.println("----------------------------------------------------------------------------------------------------------------------");
		System.out
				.println("| activity -add [Name of activity], [Category], [Time in: HH:MM], [Time out: HH:MM]      | Make an entry of activity |");
		System.out
				.println("| exit                                                                                   | Close the application     |");
		System.out
				.println("----------------------------------------------------------------------------------------------------------------------");
		System.out.println();
	}

	private static void PrintSuccessfulEntry(String Name, String Category,
			String TimeIn, String TimeOut) {
		System.out.println();
		System.out.printf("Activity Name: %s \n", Name);
		System.out.printf("Activity Name: %s \n", Category);
		System.out.printf("Time: %s to %s \n", TimeIn, TimeOut);
		System.out.println();
		System.out.println("Entry successfully added!");
		System.out.println();
	}

	// </Printing methods>

	// <Sample output>

	private static void GenerateSampleOutput() {

		ActivityModel output1 = new ActivityModel();
		ActivityModel output2 = new ActivityModel();
		ActivityModel output3 = new ActivityModel();

		output1.setActID(10000);
		output1.setActName("Batch 8 Orientation");
		output1.setActCategory("Training");
		output1.setActTimeIn("08:33");
		output1.setActTimeOut("11:20");

		output2.setActID(10001);
		output2.setActName("Lunch Out");
		output2.setActCategory("Break Time");
		output2.setActTimeIn("12:00");
		output2.setActTimeOut("13:00");

		output3.setActID(10002);
		output3.setActName("Linux");
		output3.setActCategory("Training");
		output3.setActTimeIn("15:00");
		output3.setActTimeOut("17:00");

		ActivityList.InsertSampleData(output1);
		ActivityList.InsertSampleData(output2);
		ActivityList.InsertSampleData(output3);

	}

	// </Sample output>

	private static class ActivityList {

		private final static int ID_RANGE = 10000;

		private static ArrayList<ActivityModel> activityList = new ArrayList<ActivityModel>();

		public static void InsertSampleData(ActivityModel model) {
			activityList.add(model);
		}

		public static ArrayList<ActivityModel> getActivityList() {
			return activityList;
		}

		public static void InsertEntryInActivity(String Name, String Category,
				String TimeIn, String TimeOut) {
			ActivityModel model = new ActivityModel();
			model.setActID(ID_RANGE + activityList.size());
			model.setActName(Name);
			model.setActCategory(Category);
			model.setActTimeIn(TimeIn);
			model.setActTimeOut(TimeOut);
			activityList.add(model);
		}

	}

}
