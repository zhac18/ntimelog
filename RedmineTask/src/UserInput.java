
public class UserInput {
	
	private String userName = "";
	private final char WHITE_SPACE = ' ';
	private final String USERNAME_TAG = "#: ";
	
	public UserInput(String Username){
		userName = Username;
	}
	
	public boolean validate(){ 
		boolean name = false;
		if(!userName.equals("") && !hasWhiteWhiteSpaces(userName)){
			name = true;
		}
		return name;
	}

	public String displayHeader(){
		String returningName = "";
		if (validate()){
			returningName = userName + USERNAME_TAG;
		}
		return returningName;
	}
	
	private boolean hasWhiteWhiteSpaces(String Username){
		boolean eval = false;
		char[] scanVar = Username.toCharArray();
		for (int counter = 0; counter < scanVar.length; counter++){
			if (scanVar[counter] == WHITE_SPACE){
				eval = true;
				break;
			}
		}
		return eval;
	}
	
	public String getUser(){
		return userName;
	}

}
